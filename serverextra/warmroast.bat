@echo off

echo Starting WarmRoast remote profiler...
java -Djava.library.path="C:\Program Files\Java\jdk1.7.0_51\jre\bin" -cp "C:\Program Files\Java\jdk1.7.0_51\lib\tools.jar;warmroast-1.0.0-SNAPSHOT.jar" com.sk89q.warmroast.WarmRoast --mappings "C:\Modding\mad-modpack\output\mcpmappings"
pause
