@echo off

echo Removing all previous LOG files.
del *.log

echo Removing ForgeIRC to prevent Igor from joining #MadServer on esper.net
cd mods/
del forgeirc-1.6.2-cpwbuild2.jar
cd ..

echo Running Forge server instance...
java -Xms2048m -Xmx2048m -XX:PermSize=128m -jar ForgeMod.jar nogui

echo Looking for item and block conflicts...
findconflicts.bat